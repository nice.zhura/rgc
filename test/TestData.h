#ifndef RGC_TESTDATA_H
#define RGC_TESTDATA_H

#include "Profile.h"
#include "TestRunner.h"
#include "FileReader.h"

void TestData(const std::string& test) {
  std::string polygonPath = "../test/data/" + test + "/Polygon.txt";
  std::string queryPointsPath = "../test/data/" + test + "/QueryPoints.txt";
  std::string locationsPath = "../test/data/" + test + "/Locations.txt";

  const auto polygonVertices = ReadPoints(polygonPath);
  const auto queryPoints = ReadPoints(queryPointsPath);
  const auto locations = ReadLocations(locationsPath);

  K239992::Polygon polygon(
    polygonVertices.begin(),
    polygonVertices.end()
  );
  int counter = 0;
  { // LOG_DURATION(test)
    for (int i = 0; i < queryPoints.size(); ++i) {
      if (polygon.contains(queryPoints[i]) != locations[i]) { ++counter; }
    }
  }
  const std::string hint = test + "_counter = " + to_string(counter);
  Assert(!static_cast<bool>(counter), hint);
}

void TestDataRealWorld() {
  TestData("real_world");
}

void TestDataRandom() {
  TestData("random");
}

void TestAll() {
  TestRunner tr;

  // Run Real-World Test
  tr.RunTest(TestDataRealWorld, "TestDataRealWorld");
  tr.RunTest(TestDataRandom, "TestDataRandom");
}

#endif //RGC_TESTDATA_H
