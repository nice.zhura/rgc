#ifndef RGC_FILEREADER_H
#define RGC_FILEREADER_H

#include "../src/PIP239992.h"

#include <fstream>
#include <sstream>

std::ostream& operator<<(std::ostream& os, const K239992::Point& point) {
  namespace bg = boost::geometry;

  const double point_x = bg::get<0>(point);
  const double point_y = bg::get<1>(point);

  return os << "(" << point_x << ", " << point_y << ")";
}

std::istream& operator>>(std::istream& is, K239992::Point& point) {
  namespace bg = boost::geometry;

  double point_x; double point_y;
  is >> point_x >> point_y;

  bg::set<0>(point, point_x);
  bg::set<1>(point, point_y);

  return is;
}

auto ReadPoints(const std::string& filePath) {
  std::vector<K239992::Point> points;

  std::ifstream ifs;
  ifs.open(filePath);

  if (!ifs.is_open()) { return points; }

  std::string pointLine;
  while (getline(ifs, pointLine)) {
    std::istringstream iss(pointLine);

    K239992::Point point;
    iss >> point;

    points.push_back(point);
  }

  ifs.close();
  return points;
}

auto ReadLocations(const std::string& filePath) {
  std::vector<bool> locations;

  std::ifstream ifs;
  ifs.open(filePath);

  if (!ifs.is_open()) { return locations; }

  std::string locationLine;
  while (getline(ifs, locationLine)) {
    std::istringstream iss(locationLine);

    bool location;
    iss >> location;

    locations.push_back(location);
  }

  ifs.close();
  return locations;
}

#endif //RGC_FILEREADER_H