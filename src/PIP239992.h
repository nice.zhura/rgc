#ifndef RGC_PIP239992_H
#define RGC_PIP239992_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/segment.hpp>
#include <boost/geometry/geometries/point_xy.hpp>

struct K239992;

namespace N239992 {
  namespace bg = boost::geometry;
  typedef bg::model::d2::point_xy<double> point_t;
  typedef bg::model::segment<point_t> segment_t;
  typedef bg::model::polygon<point_t> polygon_t;

  // class Point implementation
  typedef point_t Point;
  bool operator==(const Point&, const Point&);
  bool operator!=(const Point&, const Point&);

  // class Segment implementation
  typedef segment_t Segment;

  // class Polygon implementation
  class Polygon {
  public:
    Polygon() = delete;

    template<class InputIt>
    explicit Polygon(InputIt first, InputIt last) {
      const Point& closing_point = *first;
      bg::append(polygon.outer(), closing_point);
      for(++first; first != last; ++first) {
        bg::append(polygon.outer(), *first);
      }
      bg::append(polygon.outer(), closing_point);
    }

    [[nodiscard]]
    bool contains(const Point&) const;

  private:
    polygon_t polygon;
  };

} // N239992

struct K239992 {
  typedef N239992::Point Point;
  typedef N239992::Polygon Polygon;
};

#endif //RGC_PIP239992_H