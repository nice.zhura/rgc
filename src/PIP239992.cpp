#include "PIP239992.h"

namespace N239992 {
  bool operator==(const Point& lhs, const Point& rhs) {
    return (lhs.x() == rhs.x()) && (lhs.y() == rhs.y());
  }

  bool operator!=(const Point& lhs, const Point& rhs) {
    return (lhs.x() != rhs.x()) || (lhs.y() != rhs.y());
  }

  bool Polygon::contains(const Point& point) const {
    // checks if point is the vertex of the polygon
    const auto& vertices = polygon.outer();
    for (auto& vertex: vertices) {
      if (vertex == point) { return true; }
    }

    auto vertexIt = vertices.begin();
    auto nextVertexIt = ++vertices.begin();
    while (nextVertexIt != vertices.end()) {
      // point lies on line-segment
      const Segment segment(*vertexIt, *nextVertexIt);
      if (bg::covered_by(point, segment)) { return true; }

      ++vertexIt;
      ++nextVertexIt;
    }

    return bg::covered_by(point, polygon);
  }

} // N239992